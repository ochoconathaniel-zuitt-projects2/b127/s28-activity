

//3

   fetch ('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(json => console.log(json))     

//4


fetch('https://jsonplaceholder.typicode.com/todos') 
.then(res => res.json())
.then(data => {
  let list = data.map(post => {
    return post.title
  })

  console.log(list)
})

//5

fetch ('https://jsonplaceholder.typicode.com/todos/3')
  .then(response => response.json())
  .then(json => console.log(json))   

//6
  fetch ('https://jsonplaceholder.typicode.com/todos/3')
  .then(response => response.json())
  .then(json => console.log(json))

//7

  fetch('https://jsonplaceholder.typicode.com/todos', {
 
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },

  body: JSON.stringify({
    
    "userId": 1,
    "id": 201,
    "title": "delectus aut autem",
    "completed": false

  })
})
.then( res => res.json())
.then( data => console.log(data))

//8

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    "userId": 1,
    "id": 201,
    "title": "delectus aut autemasdasda",
    "completed": false
  })
})
.then(res => res.json())
.then( data => console.log(data))


//9

fetch('https://jsonplaceholder.typicode.com/posts/2', {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    "Title": "quis ut nam facilis et officia qui",
    "Description": "a poem",
    "Status": "false",
    "Date Completed": "N/A",
    "User ID": "1",
    "id": 2
  })
})
.then(res => res.json())
.then( data => console.log(data))




//10

fetch('https://jsonplaceholder.typicode.com/posts/5', {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    "title": "ang hirap",
   
  })
})
.then(res => res.json())
.then( data => console.log(data))

//11

fetch('https://jsonplaceholder.typicode.com/todos/6', {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    "completed": true,
    "date completed": "sept,15,2021"
   
  })
})
.then(res => res.json())
.then( data => console.log(data))


//12

fetch('https://jsonplaceholder.typicode.com/todos/4', {
  method: 'DELETE',

})
.then(res => res.json())
.then( data => console.log(data))